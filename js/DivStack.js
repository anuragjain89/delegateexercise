function InitializeDivElementStack() {

  function DivFactory($container, elementClass) {
    this.$container = $container;
    this.elementClass = elementClass;
  }

  DivFactory.prototype.getNextElement = function () {
    var $lastDiv = this.$container.find('div:last');
    var textForNextElement = $lastDiv.length ? parseInt($lastDiv.text(), 10) + 1 : '1';
    return $('<div>').text(textForNextElement).addClass(this.elementClass);
  };

  var container = $('.div-container');
  var button = $('.div-container-add-button');
  var elementClass = 'div-factory-element';
  var elementFactory = new DivFactory(container, elementClass);

  container.on('click', '.' + elementClass, function () {
    if ($(this).next().length) {
      $(this).addClass('highlight');
    } else {
      $(this).remove();
    }
  });

  button.on('click', {container: container, elementFactory: elementFactory}, function (event) {
    event.data.container.append(event.data.elementFactory.getNextElement());
  });

}

$(InitializeDivElementStack);